//
//  ViewController.swift
//  RockOut
//
//  Created by Joshua Armer on 11/17/15.
//  Copyright © 2015 Joshua Armer. All rights reserved.
//

import UIKit
import AVFoundation
import iAd

var messages = [
    "You Rock!",
    "Rock-N-Roll",
    "Rock Out",
    "Rocking it!",
    "Hey! People who live in glass houses should not throw rocks",
    "A rolling stone gathers no moss",
    "Rock on dude!",
    "Jesus is the Rock",
    "Hey, lets Rock around the clock?",
    "Rock-a-bye Baby!",
    "You Rock My World",
    "I feel like I'm between a Rock and a hard place",
    "On the Rocks",
    "Rock star",
    "It's going to be a smash hit",
    "Rock the boat",
    "Rock your socks off",
    "You Rock my world!",
    "Some times I feel like you take me for Granite",
    "Rock the night away",
    "Jingle bell Rock",
    "Rock around the Christmas tree",
    "Paper, Scissors, Rock",
    "Just because you Rock doesn't mean you're made of stone",
    "Sticks and Stones may break my bones, but you Rock",
    "Rock the dock",
    "You Rock! You Rule!",
    "Crockodile Rock",
    "Are you ready to Rock?",
    "Kick Rocks"]

//"Granite cause it's time to return to bustin' rocks",

var currentQuote = ""

var player:AVAudioPlayer = AVAudioPlayer()

class ViewController: UIViewController {
    
    @IBOutlet weak var message: UILabel!

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        currentQuote = messages [Int(arc4random_uniform(UInt32(messages.count)))]  // ---- Randomly select word(s) ----
        message.text = "\(currentQuote)"

        //message.text = "Rock-N-Roll!"
        canDisplayBannerAds = true
        
        // Swipe / Tap Gesture recognizer:
        let tap : Selector = "tapped:"
        let tapGesture = UITapGestureRecognizer(target: self, action: "tapped:")
        tapGesture.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapGesture)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: "swiped:")
        // This sets the direction for the swipe.
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: "swiped:")
        // This sets the direction for the swipe.
        swipeUp.direction = UISwipeGestureRecognizerDirection.Up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: "swiped:")
        // This sets the direction for the swipe.
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        
        self.view.addGestureRecognizer(swipeDown)
        
    }

    func tapped(gesture: UITapGestureRecognizer) {
        
        let alert = UIAlertController(title: "Shake it up", message: "", preferredStyle:
            UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title:"ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        self.audioPlayer()
        
    }
    
    func swiped(gesture: UIGestureRecognizer)
    {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer
        {
            // Based on the direction of swipe switch statement.
            switch swipeGesture.direction
            {
                
            case UISwipeGestureRecognizerDirection.Right:
                //println("User Swiped Right")
                let alert = UIAlertController(title: "Shake it up", message: "", preferredStyle:
                    UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title:"ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                self.audioPlayer()

            case UISwipeGestureRecognizerDirection.Left:
                //println("User Swiped Left")
                let alert = UIAlertController(title: "Shake it up", message: "", preferredStyle:
                    UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title:"ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                self.audioPlayer()
                
            case UISwipeGestureRecognizerDirection.Up:
                // println("User Swiped Up")
                let alert = UIAlertController(title: "Shake it up", message: "", preferredStyle:
                    UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title:"ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                self.audioPlayer()
                
            case UISwipeGestureRecognizerDirection.Down:
                //println("User Swiped Down")
                let alert = UIAlertController(title: "Shake it up", message: "", preferredStyle:
                    UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title:"ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                self.audioPlayer()
            default:
                break
            }
        }
    }
    
    func audioPlayer() {
        let audioPath = NSBundle.mainBundle().pathForResource("Pebbles", ofType: "aiff")!
        do {
            // This sets up our error trap.
            player = try AVAudioPlayer(contentsOfURL: NSURL(string: audioPath)!)
        } catch {
            print("error")
            //var error1 as NSError {
            // error = error1
            //soundPlayer = nil
        }
        player.prepareToPlay()
        player.play()

    }
    
    // Determine motion happenings
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        
        // Determine if it was a shake! (no, unfortunately it isn't a milkshake)
        if event!.subtype == UIEventSubtype.MotionShake
        {
            self.audioPlayer()
            currentQuote = messages [Int(arc4random_uniform(UInt32(messages.count)))]  // ---- Randomly select word(s) ----
            print("You shook me all night long! - errrm.. the device")
            message.text = "\(currentQuote)"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

