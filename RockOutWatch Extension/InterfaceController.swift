//
//  InterfaceController.swift
//  RockOutWatch Extension
//
//  Created by Joshua Armer on 11/19/15.
//  Copyright © 2015 Joshua Armer. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    var messages = [
        "You Rock!",
        "Rock-N-Roll",
        "Rock Out",
        "Rocking it!",
        "Hey! People who live in glass houses should not throw rocks",
        "A rolling stone gathers no moss",
        "Rock on dude!",
        "Jesus is the Rock",
        "Hey, lets Rock around the clock?",
        "Rock-a-bye Baby!",
        "You Rock My World",
        "I feel like I'm between a Rock and a hard place",
        "On the Rocks",
        "Rock star",
        "It's going to be a smash hit",
        "Rock the boat",
        "Rock your socks off",
        "You Rock my world!",
        "Some times I feel like you take me for Granite",
        "Rock the night away",
        "Jingle bell Rock",
        "Rock around the Christmas tree",
        "Paper, Scissors, Rock",
        "Just because you Rock doesn't mean you're made of stone",
        "Sticks and Stones may break my bones, but you Rock",
        "Rock the dock",
        "You Rock! You Rule!",
        "Crockodile Rock",
        "Are you ready to Rock?",
        "Kick Rocks"]
    
    //"Granite cause it's time to return to bustin' rocks",
    
    var currentQuote = ""
    
    
    @IBOutlet var message: WKInterfaceLabel!

    @IBAction func shakeItUp() {
        currentQuote = messages [Int(arc4random_uniform(UInt32(messages.count)))]  // ---- Randomly select word(s) ----
        message.setText("\(currentQuote)")
        
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        currentQuote = messages [Int(arc4random_uniform(UInt32(messages.count)))]  // ---- Randomly select word(s) ----       
        message.setText("\(currentQuote)")
        
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
